package com.gestiontweets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gestiontweets.controller.GestionTweetsRest;
import com.gestiontweets.model.SimulatorTweet;

@RunWith(SpringRunner.class)
@WebMvcTest(value = GestionTweetsRest.class)
class GestionTweetsApplicationTests {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testCrearTweets() throws Exception {
		
		SimulatorTweet tw = new SimulatorTweet();
		tw.setSeguidores(2000);
		tw.setLang("es");
		tw.setUsuario("Fernando");
		tw.setTexto("Prueba");
		tw.setLocalizacion("San Diego");
		tw.setHashtags("#hormiguero");
		tw.setValidacion(false);
					
		String inputInJson = this.mapToJson(tw);
		String URI = "/tweet/CrearTweets";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
				.content(inputInJson).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		String outPutInJson = response.getContentAsString();
			
		assertThat(outPutInJson).isEqualTo(inputInJson);
	}
	
	@Test
	void testCrearTweets1() throws Exception {
		
		SimulatorTweet tw = new SimulatorTweet();
		tw.setSeguidores(2000);
		tw.setLang("es");
		tw.setUsuario("Manuel");
		tw.setTexto("Prueba1");
		tw.setLocalizacion("Madrid");
		tw.setHashtags("#tu cara me suena");
		tw.setValidacion(false);
					
		String inputInJson = this.mapToJson(tw);
		String URI = "/tweet/CrearTweets";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
				.content(inputInJson).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		String outPutInJson = response.getContentAsString();
			
		assertThat(outPutInJson).isEqualTo(inputInJson);
	}
	
	@Test
	void testCrearTweets2() throws Exception {
		
		SimulatorTweet tw = new SimulatorTweet();
		tw.setSeguidores(2000);
		tw.setUsuario("Jaime");
		tw.setLang("es");
		tw.setTexto("Prueba2");
		tw.setLocalizacion("Barcelona");
		tw.setHashtags("#tucaramesuena");
		tw.setValidacion(false);
					
		String inputInJson = this.mapToJson(tw);
		String URI = "/tweet/CrearTweets";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
				.content(inputInJson).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		String outPutInJson = response.getContentAsString();
			
		assertThat(outPutInJson).isEqualTo(inputInJson);
	}
	
	@Test
	void testCrearTweets3() throws Exception {
		
		SimulatorTweet tw = new SimulatorTweet();
		tw.setSeguidores(2000);
		tw.setUsuario("Ana Maria");
		tw.setLang("en");
		tw.setTexto("Prueba3");
		tw.setLocalizacion("Valencia");
		tw.setHashtags("#Baloncesto");
		tw.setValidacion(false);
					
		String inputInJson = this.mapToJson(tw);
		String URI = "/tweet/CrearTweets";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
				.content(inputInJson).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		String outPutInJson = response.getContentAsString();
			
		assertThat(outPutInJson).isEqualTo(inputInJson);
	}
	
	@Test
	void testCrearTweets4() throws Exception {
		
		SimulatorTweet tw = new SimulatorTweet();
		tw.setSeguidores(500);
		tw.setLang("es");
		tw.setUsuario("Ramiro");
		tw.setTexto("Prueba4");
		tw.setLocalizacion("Boston");
		tw.setHashtags("#hormiguero");
		tw.setValidacion(false);
					
		String inputInJson = this.mapToJson(tw);
		String URI = "/tweet/CrearTweets";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
				.content(inputInJson).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		String outPutInJson = response.getContentAsString();
			
		assertThat(outPutInJson).isEqualTo(inputInJson);
	}
	
	@Test
	void testCrearTweets5() throws Exception {
		
		SimulatorTweet tw = new SimulatorTweet();
		tw.setSeguidores(2000);
		tw.setUsuario("Maria");
		tw.setLang("es");
		tw.setTexto("Prueba5");
		tw.setLocalizacion("La Rioja");
		tw.setHashtags("#Sergio Dalma");
		tw.setValidacion(false);
					
		String inputInJson = this.mapToJson(tw);
		String URI = "/tweet/CrearTweets";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
				.content(inputInJson).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		String outPutInJson = response.getContentAsString();
			
		assertThat(outPutInJson).isEqualTo(inputInJson);
	}
	
	@Test
	void testCrearTweets6() throws Exception {
		
		SimulatorTweet tw = new SimulatorTweet();
		tw.setSeguidores(2000);
		tw.setUsuario("Alberto");
		tw.setLang("es");
		tw.setTexto("Prueba6");
		tw.setLocalizacion("Asturias");
		tw.setHashtags("#Camino de Santiago");
		tw.setValidacion(false);
					
		String inputInJson = this.mapToJson(tw);
		String URI = "/tweet/CrearTweets";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
				.content(inputInJson).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		String outPutInJson = response.getContentAsString();
			
		assertThat(outPutInJson).isEqualTo(inputInJson);
	}
	
	@Test
	void testCrearTweets7() throws Exception {
		
		SimulatorTweet tw = new SimulatorTweet();
		tw.setSeguidores(2000);
		tw.setUsuario("Raul");
		tw.setLang("es");
		tw.setTexto("Prueba7");
		tw.setLocalizacion("Madrid");
		tw.setHashtags("#Camino de Santiago");
		tw.setValidacion(false);
					
		String inputInJson = this.mapToJson(tw);
		String URI = "/tweet/CrearTweets";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
				.content(inputInJson).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		String outPutInJson = response.getContentAsString();
			
		assertThat(outPutInJson).isEqualTo(inputInJson);
	}
	
	@Test
	void marcarValidado() throws Exception {
					
		String URI = "/tweet/Alberto";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
				.content("").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		String outPutInJson = response.getContentAsString();
			
		assertEquals(200,result.getResponse().getStatus());
	}
	
	@Test
	void consultarTweets() throws Exception {
	
		String URI = "/tweet/consultarTweets";
				
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).content("")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_VALUE);
	
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
	
		String outPutInJson = result.getResponse().getContentAsString();
		
		System.out.println("outPutInJson "+outPutInJson);
						
		assertEquals(200,result.getResponse().getStatus());
		
	}
	
	@Test
	void consultarTweetsValidados() throws Exception {
	
		String URI = "/tweet/consultarTweetsValidados";
				
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).content("")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_VALUE);
	
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
	
		String outPutInJson = result.getResponse().getContentAsString();
		
		System.out.println("outPutInJson "+outPutInJson);
						
		assertEquals(200,result.getResponse().getStatus());
		
	}
	
	@Test
	void consultarHashtags() throws Exception {
	
		String URI = "/tweet/consultarHashtags";
				
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).content("")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_VALUE);
	
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
	
		String outPutInJson = result.getResponse().getContentAsString();
		
		System.out.println("outPutInJson "+outPutInJson);
						
		assertEquals(200,result.getResponse().getStatus());
		
	}
	
	private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}
