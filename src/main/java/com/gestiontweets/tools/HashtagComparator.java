package com.gestiontweets.tools;

import java.util.Comparator;
import com.gestiontweets.model.Hashtag;

public class HashtagComparator implements Comparator<Hashtag> {
	@Override
	public int compare(Hashtag o1, Hashtag o2) {
		return new Integer(o1.getContador()).compareTo(new Integer(o2.getContador()));
	}
}
