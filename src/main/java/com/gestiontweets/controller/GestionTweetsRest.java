package com.gestiontweets.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gestiontweets.common.Constants;
import com.gestiontweets.model.Hashtag;
import com.gestiontweets.model.SimulatorTweet;
import com.gestiontweets.tools.HashtagComparator;

@RestController
@RequestMapping(GestionTweetsRest.TWEET)
public class GestionTweetsRest {
	public static final String TWEET = "/tweet";
			
	List<SimulatorTweet> tweets = new ArrayList<SimulatorTweet>();
	boolean idioma = false;
	int cont = 0;
	String hashtag = "";
	private boolean isIdioma() {
		return idioma;
	}

	private void setIdioma(boolean idioma) {
		this.idioma = idioma;
	}

	//-->Crear Tweets
	@PostMapping("/CrearTweets")
	public SimulatorTweet crearTweets(@RequestBody SimulatorTweet twSimulador) {	
					
		String lang  = twSimulador.getLang();
					
		List<String> listIdiomas = Constants.listIdiomas;
		listIdiomas.add("es");
		listIdiomas.add("fr");
		listIdiomas.add("it");

		listIdiomas.stream().filter(s->s.equals(lang)).forEach(s->setIdioma(true));
		
		if(isIdioma() && twSimulador.getSeguidores()>Constants.SEGUIDORES) {
			tweets.add(twSimulador);		
		}
		
		return twSimulador;
	}
	
	//-->Consultar los tweets
	@GetMapping("/consultarTweets")
	public List<SimulatorTweet> consultarTweets() {		
		return tweets;
	}	
	
	//-->Marcar un tweet como validado
	@PostMapping(value="/{usuario}")
	public void marcarValidado(@PathVariable("usuario") String usuario) {
		SimulatorTweet tw =  new SimulatorTweet();	
		for(int i=0;i<tweets.size();i++) {
			tw = tweets.get(i);
			if(tw.getUsuario().equals(usuario)) {
				tw.setValidacion(true);
				return;
			}				
		}				
	}
				
	//-->Consultar los tweets validados por usuario
	@GetMapping("/consultarTweetsValidados")
	public List<SimulatorTweet>  consultarTweetsValidados(){
		SimulatorTweet tw =  new SimulatorTweet();		
		List<SimulatorTweet> tweetsValidados = new ArrayList<SimulatorTweet>();
						
		for(int i=0;i<tweets.size();i++) {
			tw = tweets.get(i);
			if (tw.isValidacion()) {				
				tweetsValidados.add(tw);
			}

		}			
		return tweetsValidados;				
	}
		
	//-->Consultar una clasificación de los N hashtags más usados (default 10).
	@GetMapping("/consultarHashtags")
	public List<Hashtag> consultarHashtags(){
		
		List<Hashtag> hashtagMasUsados = new ArrayList<Hashtag>();
		
		SimulatorTweet tw =  new SimulatorTweet();		
		Hashtable ha= new Hashtable<>();	
		for(int i = 0; i<tweets.size();i++) {
			tw = tweets.get(i);
			hashtag = tw.getHashtags();
			tweets.stream().filter(s->s.getHashtags().equals(hashtag)).forEach(s->cont++);	
			ha.put(hashtag,""+cont);			
			cont = 0;
		}
		
		Enumeration e = ha.keys();
		Object clave;
		Object valor;
		while( e.hasMoreElements() ){
		  clave = e.nextElement();
		  valor = ha.get( clave );
		  Hashtag hastag = new Hashtag();
		  hastag.setHashtag(clave.toString());
		  hastag.setContador(valor.toString());
		  hashtagMasUsados.add(hastag);		  
		}
		Collections.sort(hashtagMasUsados, new HashtagComparator().reversed());
		return hashtagMasUsados;
	}   
}
