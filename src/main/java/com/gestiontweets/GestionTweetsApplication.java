package com.gestiontweets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionTweetsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionTweetsApplication.class, args);
	}

}
