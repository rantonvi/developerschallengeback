package com.gestiontweets.common;

import java.util.ArrayList;
import java.util.List;

public class Constants {
	
	public static final int SEGUIDORES = 1500;
	public static final int HASHTAGS   = 10;
	public static List<String> listIdiomas = new ArrayList<String>();
}
